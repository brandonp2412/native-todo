import { registerRootComponent } from "expo";
import FileViewer from "react-native-file-viewer";
import "react-native-gesture-handler";
import PushNotification from "react-native-push-notification";
import App from "./src/App";
import { NOTIFICATION_CHANNEL_ID } from "./src/constants";

PushNotification.configure({
  onNotification(notification) {
    console.log(`Opening file: ${notification.data.path}`);
    FileViewer.open(notification.data.path).then(() => {});
  },
  requestPermissions: false,
});

PushNotification.createChannel(
  {
    channelId: NOTIFICATION_CHANNEL_ID, // (required)
    channelName: "Native TODO", // (required)
    channelDescription: "Notifications sent by the Native TODO application.", // (optional) default: undefined.
    soundName: "default", // (optional) See `soundName` parameter of `localNotification` function
    importance: 3, // (optional) default: 4. Int value of the Android notification importance
    vibrate: false, // (optional) default: true. Creates the default vibration patten if true.
  },
  (created) =>
    console.log(
      created
        ? "Created new notification channel."
        : "Notification channel already exists."
    ) // (optional) callback returns whether the channel was created, false means it already existed.
);

// registerRootComponent calls AppRegistry.registerComponent('main', () => App);
// It also ensures that whether you load the app in the Expo client or in a native build,
// the environment is set up appropriately
registerRootComponent(App);
