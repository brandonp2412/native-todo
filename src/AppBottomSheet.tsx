import AsyncStorage from "@react-native-async-storage/async-storage";
import { StackNavigationProp } from "@react-navigation/stack";
import React from "react";
import DocumentPicker from "react-native-document-picker";
import { BottomSheet, ListItem } from "react-native-elements";
import RNFS from "react-native-fs";
import PushNotification from "react-native-push-notification";
import { NOTIFICATION_CHANNEL_ID } from "./constants";
import { Routes } from "./routes";
import { getTodo, getTodos } from "./todo.service";

const AppBottomSheet = ({
  navigation,
  visible,
  setVisible,
}: {
  navigation: StackNavigationProp<Routes, "Home">;
  visible: boolean;
  setVisible: (visible: boolean) => void;
}) => {
  return (
    <BottomSheet isVisible={visible} modalProps={{}}>
      <ListItem
        onPress={async () => {
          setVisible(false);
          const path = RNFS.ExternalDirectoryPath + "/todos.txt";
          let todos = await getTodos();
          todos = await Promise.all(
            todos.map(async (title) => {
              const items = await getTodo(title);
              const body = items.map((item) => `- ${item}`).join("\n");
              return `${title}:\n` + body;
            })
          );
          console.log(`Writing path: ${path}`);
          await RNFS.writeFile(path, todos.join("\n\n"));
          console.log("Sending push notification.");
          PushNotification.localNotification({
            vibrate: false,
            importance: "low",
            message: "Downloaded TODOs",
            channelId: NOTIFICATION_CHANNEL_ID,
            id: 1,
            userInfo: {
              path,
            },
          });
        }}
      >
        <ListItem.Content>
          <ListItem.Title>Download</ListItem.Title>
        </ListItem.Content>
        <ListItem.Chevron name="file-download"></ListItem.Chevron>
      </ListItem>
      <ListItem
        onPress={async () => {
          console.log("Picking document...");
          const res = await DocumentPicker.pick({
            type: [DocumentPicker.types.plainText],
          });
          const data = await RNFS.readFile(res.fileCopyUri);
          console.log({ data });
          const sets = data
            .split("\n\n")
            .map((d) => d.split(":\n"))
            .map(async (todo) => {
              const key = todo[0].replace(/:|\n/, "");
              console.log({ key });
              const value = todo[1]
                .split("\n")
                .map((t) => t.replace(/^- /, ""));
              console.log({ value });
              await AsyncStorage.setItem(key, JSON.stringify(value));
            });
          await Promise.all(sets);
          console.log("Focusing home...");
          navigation.reset({ routes: [{ name: "Home" }] });
          setVisible(false);
        }}
      >
        <ListItem.Content>
          <ListItem.Title>Upload</ListItem.Title>
        </ListItem.Content>
        <ListItem.Chevron name="file-upload"></ListItem.Chevron>
      </ListItem>
      <ListItem onPress={() => setVisible(false)}>
        <ListItem.Content>
          <ListItem.Title>Cancel</ListItem.Title>
        </ListItem.Content>
        <ListItem.Chevron name="cancel"></ListItem.Chevron>
      </ListItem>
    </BottomSheet>
  );
};

export default AppBottomSheet;
