import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import React, { useState } from "react";
import { View } from "react-native";
import { Button, Icon } from "react-native-elements";
import AppBottomSheet from "./AppBottomSheet";
import Home from "./Home";
import ViewTodo from "./ViewTodo";

const Stack = createStackNavigator();

const options = {
  headerStyle: {
    backgroundColor: "#2e3535",
    borderBottomWidth: 0,
  },
  headerTintColor: "white",
};

export default function App() {
  const [visible, setVisible] = useState(false);

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={Home}
          options={({ navigation }) => ({
            ...options,
            title: "🏠 Home",
            headerRight: () => (
              <View style={{ marginRight: 10 }}>
                <Button
                  onPress={() => setVisible(true)}
                  type="clear"
                  icon={<Icon color="white" name="more" />}
                />
                <AppBottomSheet
                  navigation={navigation}
                  visible={visible}
                  setVisible={setVisible}
                />
              </View>
            ),
          })}
        />
        <Stack.Screen
          name="ViewTodo"
          component={ViewTodo}
          options={({ route }) => ({
            ...options,
            headerRight: () => <View style={{ marginRight: 10 }}></View>,
            title: route.params?.title,
          })}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
