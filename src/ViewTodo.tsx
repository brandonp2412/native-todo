import AsyncStorage from "@react-native-async-storage/async-storage";
import { RouteProp } from "@react-navigation/native";
import React, { useEffect, useState } from "react";
import { StyleSheet, ToastAndroid, View } from "react-native";
import { Input, ListItem } from "react-native-elements";
import { Routes } from "./routes";
import { getTodo } from "./todo.service";

export default function ViewTodo({
  route,
}: {
  route: RouteProp<Routes, "ViewTodo">;
}) {
  const [items, setItems] = useState<string[]>([]);
  const [newItem, setNewItem] = useState<string>();

  useEffect(() => {
    getTodo(route.params.title).then((_items) => setItems(_items));
  }, [route.params.title]);

  useEffect(() => {
    AsyncStorage.setItem(route.params.title, JSON.stringify(items)).then();
  }, [items]);

  const addItem = (text: string) => {
    console.log(`Adding an item: ${text}`);
    setItems([...items, text]);
    setNewItem("");
  };

  const remove = (text: string) => {
    setItems(items.filter((item) => item !== text));
    ToastAndroid.show(`${text} ✔️`, ToastAndroid.SHORT);
  };

  return (
    <View style={styles.container}>
      <Input
        placeholder="New item..."
        onSubmitEditing={(e) => addItem(e.nativeEvent.text)}
        blurOnSubmit={false}
        onChangeText={(text) => setNewItem(text)}
        value={newItem}
      />
      {items.map((item) => (
        <ListItem key={item} bottomDivider onPress={() => remove(item)}>
          <ListItem.Content>
            <ListItem.Title>{item}</ListItem.Title>
          </ListItem.Content>
          <ListItem.Chevron name="check" />
        </ListItem>
      ))}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    backgroundColor: "#f9c2ff",
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});
