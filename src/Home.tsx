import AsyncStorage from "@react-native-async-storage/async-storage";
import { StackNavigationProp } from "@react-navigation/stack";
import React, { useEffect, useState } from "react";
import {
  NativeSyntheticEvent,
  Text,
  TextInputSubmitEditingEventData,
  ToastAndroid,
  View,
} from "react-native";
import { Input, ListItem } from "react-native-elements";
import Swipeable from "react-native-gesture-handler/Swipeable";
import Example from "./Example";
import { Routes } from "./routes";
import { getTodos } from "./todo.service";

export default function Home({
  navigation,
  setVisible,
}: {
  navigation: StackNavigationProp<Routes, "Home">;
}) {
  const [todos, setTodos] = useState<string[]>([]);
  const [newTodo, setNewTodo] = useState("");

  useEffect(() => {
    return navigation.addListener("focus", async () => {
      setTodos(await getTodos());
    });
  }, [navigation]);

  const addTodo = async (text: string) => {
    setTodos([...todos, text]);
    await AsyncStorage.setItem(text, "[]");
  };

  const remove = async (item: string) => {
    console.log(`Removing: ${item}`);
    setTodos(todos.filter((todo) => todo !== item));
    ToastAndroid.show(`🗑️ ${item}`, ToastAndroid.SHORT);
    await AsyncStorage.removeItem(item);
  };

  const submit = async (
    e: NativeSyntheticEvent<TextInputSubmitEditingEventData>
  ) => {
    await addTodo(e.nativeEvent.text);
    setNewTodo("");
  };

  const goTo = (todo: string) => {
    console.log(`Viewing todo: ${todo}`);
    navigation.navigate("ViewTodo", { title: todo });
  };

  return (
    <View>
      <Example />
      <Input
        placeholder="New todo..."
        blurOnSubmit={false}
        onSubmitEditing={submit}
        value={newTodo}
        onChangeText={(text) => setNewTodo(text)}
      />

      {todos.map((todo) => (
        <Swipeable
          renderLeftActions={() => <Text> </Text>}
          onSwipeableLeftWillOpen={() => remove(todo)}
          key={todo}
        >
          <ListItem bottomDivider onPress={() => goTo(todo)}>
            <ListItem.Content>
              <ListItem.Title>{todo}</ListItem.Title>
            </ListItem.Content>
            <ListItem.Chevron />
          </ListItem>
        </Swipeable>
      ))}
    </View>
  );
}
