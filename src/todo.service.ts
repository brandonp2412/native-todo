import AsyncStorage from "@react-native-async-storage/async-storage";

export async function getTodos() {
  const keys = await AsyncStorage.getAllKeys();
  return keys.filter((key) => !key.match(/EXPO_[\w_]+/));
}

export async function getTodo(title: string): Promise<string[]> {
  const item = await AsyncStorage.getItem(title);
  return JSON.parse(item!);
}
