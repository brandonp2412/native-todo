import { StackNavigationProp } from "@react-navigation/stack";
import React from "react";
import { Button, Pressable, StyleSheet, View } from "react-native";
import { Routes } from "./routes";

export default function Todo({
  item,
  navigation,
  onRemove,
}: {
  item: string;
  navigation: StackNavigationProp<Routes, "Home">;
  onRemove: (item: string) => void;
}) {
  return (
    <View style={todoStyles.container}>
      <Pressable onLongPress={() => onRemove(item)}>
        <Button
          title={item}
          onPress={() => navigation.navigate("ViewTodo", { title: item })}
        ></Button>
      </Pressable>
    </View>
  );
}

export const todoStyles = StyleSheet.create({
  container: {
    padding: 5,
  },
});
