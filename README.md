# Native TODO

This is a [React Native](https://reactnative.dev) application for tracking
tasks.

Initially, this project was scaffolded with [Expo](https://expo.io), but
was ultimately ejected.

# Installation

This project uses [Yarn](https://yarnpkg.com/).

```sh
yarn install
```

# Local development

To launch this project first you need a native webserver running:

```sh
npx react-native start
```

Then, you can run on whichever device you prefer:

```sh
npx react-native run-android
npx react-native run-ios
```

# Packaging for Production

To test the release build of the application, after [setting up for release](https://reactnative.dev/docs/signed-apk-android) you can run:

```sh
npx react-native run-android --variant=release
```

If you want to produce the APK, you can run

```sh
./gradlew bundleRelease
```

# Relevant Documentation

- [Publishing to google play store](https://reactnative.dev/docs/signed-apk-android)
- [Expo documentation](https://docs.expo.io/)
- [React Native documentation](https://reactnative.dev/docs/getting-started)
- [React documentation](https://reactjs.org/docs/getting-started.html)
